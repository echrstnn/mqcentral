#include "mqcentral.hpp"

static void test_cb(const std::string& , const std::string& )
{
}

extern "C"
{
    char MODULE_NAME[] = "test2";

    int init()
    {
        MQC.register_callback("db", test_cb);
        return 0;
    }
}
