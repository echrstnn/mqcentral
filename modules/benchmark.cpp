#include <algorithm>
#include <condition_variable>
#include <unistd.h>
#include <vector>

#include "mqcentral.hpp"
#include "db.hpp"

#define SIZE_SMALL      64
#define SIZE_BIG   1000000

#define NUM_TOPICS 10000

#define NUM_INSERT 8000000
#define NUM_GET    2000000

#define NUM_PINGS 40

#define RANDOM_SZ 100000
static std::string randomstr[RANDOM_SZ];

using Clock = std::chrono::high_resolution_clock;

std::condition_variable cv;
Clock::time_point ping2 = Clock::now();

static void ping_cb(const std::string&, const std::string&)
{
    ping2 = Clock::now();
    cv.notify_one();
}

static void benchmark_mqtt()
{
    log_info("Benchmarking MQTT");
    StackLogger _;

    std::chrono::duration<double> dur;
    double total = 0;
    std::vector<double> pings;

    usleep(3000 * 1000);

    for(int i = 0; i < NUM_PINGS; i++)
    {
        usleep(127 * 1000);

        std::mutex mutex;
        std::unique_lock<std::mutex> lock(mutex);

        auto ping1 = Clock::now();
        MQC.publish("ping", "msg");

        cv.wait(lock);

        dur = ping2 - ping1;
        double ms = dur.count() * 1000;
        log_info("Ping %d: %.2lfms", i+1, ms);

        total += ms;

        pings.push_back(ms);
    }

    std::sort(pings.begin(), pings.end());

    log_notice("MQTT ping results:");
    log_notice("  - avg %.2lfms", total / NUM_PINGS);
    log_info  ("  - min %.2lfms", pings[0]);
    log_info  ("  - Q1  %.2lfms", pings[NUM_PINGS / 4]);
    log_info  ("  - Q2  %.2lfms", pings[NUM_PINGS / 2]);
    log_info  ("  - Q3  %.2lfms", pings[(3 * NUM_PINGS) / 4]);
    log_info  ("  - max %.2lfms", pings[NUM_PINGS - 1]);
}

static void benchmark()
{
    log_info("Benchmark starting");
    StackLogger _;

    for(int i = 0; i < RANDOM_SZ; i++)
        randomstr[i] = std::to_string(std::rand() % NUM_TOPICS);

    // DB.get()

    log_info("Benchmarking DB.get()");
    auto t1 = Clock::now();

    for(int i = 0; i < NUM_GET; i++)
        DB.get(randomstr[i % RANDOM_SZ]);

    auto t2 = Clock::now();
    std::chrono::duration<double> s = t2 - t1;
    log_notice("DB.get(): %d op in %.4lfs (%.0lfk/s)", NUM_GET, s.count(), NUM_GET / s.count() / 1000);

    // Overall insert

    log_info("Benchmarking insert_overall()");
    LockedDBTopic dbt = DB.get("big");
    t1 = Clock::now();

    for(int i = 0; i < NUM_INSERT; i++)
    {
        DBEntry e;
        e.msg = randomstr[i % RANDOM_SZ];
        time(&e.time);
        dbt->insert_overall(e);
    }

    t2 = Clock::now();
    s = t2 - t1;
    log_notice("insert_overall(): %d op in %.4lfs (%.0lfk/s)", NUM_INSERT, s.count(), NUM_INSERT / s.count() / 1000);

    // insert_circular

    log_info("Benchmarking insert_circular()");
    t1 = Clock::now();

    for(int i = 0; i < NUM_INSERT; i++)
    {
        DBEntry e;
        e.msg = randomstr[i % RANDOM_SZ];
        time(&e.time);
        dbt->insert_circular(e);
    }

    t2 = Clock::now();
    s = t2 - t1;
    log_notice("insert_circular(): %d op in %.4lfs (%.0lfk/s)", NUM_INSERT, s.count(), NUM_INSERT / s.count() / 1000);

    // get_overall()

    log_info("Benchmarking get_overall()");
    t1 = Clock::now();
    int nop = dbt->size_overall();

    for(int i = 0; i < nop; i++)
        dbt->get_overall(i);

    t2 = Clock::now();
    s = t2 - t1;
    log_notice("get_overall(): %d op in %.4lfs (%.0lfk/s)", nop, s.count(), nop / s.count() / 1000);

    // get_circular()

    log_info("Benchmarking get_circular()");
    t1 = Clock::now();
    nop = dbt->size_circular();

    for(int i = 0; i < nop; i++)
        dbt->get_circular(i);

    t2 = Clock::now();
    s = t2 - t1;
    log_notice("get_circular(): %d op in %.4lfs (%.0lfk/s)", nop, s.count(), nop / s.count() / 1000);

    // MQTT

    benchmark_mqtt();

    log_info("Benchmark finished, quitting.");
    log_warn("It is recommanded to remove DB after benchmarking");
    exit(EXIT_SUCCESS);
}

extern "C"
{
    char MODULE_NAME[] = "benchmark";

    int init()
    {
        for(int i = 0; i < NUM_TOPICS; i++)
            DB.register_circular(std::to_string(i), SIZE_SMALL);

        DB.register_overall("big", SIZE_BIG);
        DB.register_circular("big", SIZE_BIG);

        MQC.register_callback("ping", ping_cb);

        MQC.register_on_connect(benchmark);
        return 0;
    }
}
