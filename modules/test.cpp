#include "mqcentral.hpp"
#include "db.hpp"

static void test_cb(const std::string& topic, const std::string& msg)
{
    log_info("[%s]: %s", topic.c_str(), msg.c_str());
    int i = std::atoi(msg.c_str());
    i++;
    std::string newmsg = std::to_string(i);
    MQC.publish("test", newmsg);
}

extern "C"
{
    char MODULE_NAME[] = "test";

    int init()
    {
        MQC.register_callback("test", test_cb);
        DB.register_overall("regov");
        DB.register_overall("ov16", 16);
        DB.register_circular("cc");
        DB.register_circular("c24", 24);
        return 0;
    }
}
