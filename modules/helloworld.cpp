#include "mqcentral.hpp"

static void say_hello(const std::string& topic, const std::string& msg)
{
    log_info("Saying Hello world because %s received on topic %s", msg.c_str(), topic.c_str());

    MQC.publish("hello/answer", "helloworld");
}

extern "C"
{
    char MODULE_NAME[] = "Hello world";

    int init()
    {
        MQC.register_callback("hello/say", say_hello);
        return 0;
    }
}
