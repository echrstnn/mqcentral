# MQCentral

MQCentral is a MQTT automation tool.

It is an evolution of my first (discontinued) MQTT automation project homesw. It achieves the same goal but with a totally different design.


# Table of contents

<!--ts-->
   * [MQCentral](#mqcentral)
   * [Table of contents](#table-of-contents)
   * [Introduction](#introduction)
      * [Working principle](#working-principle)
      * [Project's story](#projects-story)
      * [Features](#features)
   * [Tutorial](#tutorial)
   * [API documentation](#api-documentation)
   * [Performance](#performance)
      * [Benchmark](#benchmark)
      * [Implementation details](#implementation-details)

<!-- Added by: ban, at: Wed Apr  3 18:10:39 CEST 2019 -->

<!--te-->

# Introduction
## Working principle

MQCentral is a tool for MQTT automation. It simplifies the way to accomplish this. MQCentral is based on a core-modules design. Modules are written by the user and describe the automation scenario. This let the user write easy and simple scenario as well as very complex ones. Modules are written in C++ then compiled and dynamically loaded by MQCentral. This design provide a full access to language provided loops, conditionnal structures and the STL, without having to deal with a custom "language" to write scenarios like in many others projects. It also provides a very good performance since modules are compiled and the core uses efficients data structures.

Here is the `Hello world` module publishing `helloworld` to topic `hello/answer` when receiving a message to `hello/say`:

```cpp
#include "mqcentral.hpp"

static void say_hello(const std::string& topic, const std::string& msg)
{
    log_info("Saying Hello world because %s received on topic %s", msg.c_str(), topic.c_str());

    MQC.publish("hello/answer", "helloworld");
}

extern "C"
{
    char MODULE_NAME[] = "Hello world";

    int init()
    {
        MQC.register_callback("hello/say", say_hello);
        return 0;
    }
}
```

## Features

- [x] Core-modules design
- [x] DB storing messages
- [x] Running callbacks in separate threads
- [x] DB size limit and smart compression (overall trend mode)
- [x] Two TB modes
  - [x] Circular buffer
  - [x] Overall trend
- [x] Topics to access time and date
- [x] Log system
- [x] Can operate as background daemon


# Getting started
## Installation

To begin install `cmake` and `mosquitto`. Assuming you wish install MQCentral on a Raspberry Pi running under Raspbian:

```shell
sudo apt-get install cmake mosquitto mosquitto-clients libmosquittopp-dev
```

Then clone the repo, build, install and create a new config file and DB

```shell
git clone https://gitlab.com/echrstnn/mqcentral
cd mqcentral
./install.sh
sudo mqcentral --newconf --newdb
```

## First test

Now we are ready for the first test. You will need two terminals (or ssh sessions). So in the first terminal use a MQTT client to subscribe to topic `helloworld/answer`:

```shell
mosquitto_sub -t hello/answer
```

In the second one start MQCentral, and use a client to publish a message in topic `hello/say`. Then stop MQCentral by pulishing `1` in `mqc/exit` topic. You should receive a message `helloworld` in the first terminal if everything worked. If not, you may want to check system logs or trying again in verbose mode (`mqcentral -v`).

```shell
sudo mqcentral
mosquitto_pub -t hello/say -m test
mosquitto_pub -t mqc/exit -m 1
```

## Inspect logs

You can run MQCentral in foreground with `--foreground` (or `-F` shortcut) option, logs will be printed in realtime in console.

If MQCentral is runned in background, logs are sent to syslog. To see logs use `journalctl`:

```shell
journalctl SYSLOG_IDENTIFIER=mqcentral -o cat -e          # Display all logs using less
journalctl SYSLOG_IDENTIFIER=mqcentral -o cat -n 50 -f    # Or follow new messages in real time
```

You may want to add aliases in you `.bashrc`:

```shell
echo "alias mqclogs='journalctl SYSLOG_IDENTIFIER=mqcentral -o cat'" >> ~/.bashrc
source ~/.bashrc
# mqclogs -e or mqclogs -f
```

## Configuration

The config file is located to `/etc/mqcentral.conf` by default but you can use `--file` (or `-f` shortcut) if you want to use a confg file located somewhere else.

```
{
    "m_host": "localhost",
    "m_port": 1883,
    "m_db_path": "/var/lib/mqcentral.db",
    "m_modules_path": [
        "/path/to/module1.so",
        "/path/to/module2.so",
        ... ,
        "/path/to/moduleN.so"
    ]
}
```

The config file is in JSON format. The main parameter to change is the list of modules. During startup MQCentral will attempt to load all modules `.so` files specified.

# API documentation
# Performance
## Benchmark

To benchmark MQCentral performance, use benchmark module.

You have to use a new temporary config file with a new DB because benchmarking will add a lot of useless data in DB.

So, create a new config file `/tmp/bench.conf`:

```
{
    "m_host": "localhost",
    "m_port": 1883,
    "m_db_path": "/tmp/bench.db",
    "m_modules_path": [
        "/path/to/mqcmodule-benchmark.so"
    ]
}
```

Generate a new db then launch benchmark (run MQCentral in foreground):

```shell
sudo mqcentral -f /tmp/bench.conf --newdb
sudo mqcentral -f /tmp/bench.conf -F
```

What is benchmarked:
- `DB.get()`: Operation to find and lock a DB topic.
- `insert_overall()`: Insert in overall mode
- `insert_circular()`: Insert in circular mode
- `get_overall()`: Get from overall mode
- `get_circular()`: Get from circular mode
- MQTT (added soon)

Reapeating a couple benchmarks (remove and regen a new DB each time) gave me the following values with a Raspberry Pi 3.

| Operation | Times per second |
| :------: | :------: |
| `DB.get()` | 370 000 |
| `insert_overall()` | 1 250 000 |
| `insert_circular()` | 1 000 000 |
| `get_overall()` | 12 200 000 |
| `get_circular()` | 7 300 000 |

We can see that the slowest operation is finding and locking a DB topic, even if it still very fast for the dataset in benchmark (benchmarked with 10 000 differents DB topics).

Db size

## Implementation details
