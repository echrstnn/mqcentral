#ifndef MQC_MQCENTRAL_H
#define MQC_MQCENTRAL_H

#include <map>
#include <mosquittopp.h>
#include <set>

#include "logger.hpp"
#include "version.hpp"


typedef int (*init_func_t)();
typedef void (*func_t)();
typedef void (*callback_func_t)(const std::string&, const std::string&);


class MQCentral : public mosqpp::mosquittopp
{
    public:

    // Safe to use

    void register_on_connect    (func_t func);
    void register_on_disconnect (func_t func);
    void register_callback      (const std::string& topic, callback_func_t func);

    void publish(const std::string& topic, const std::string& msg, bool retain = false);

    // Internal usage, recommanded to do not use in modules

    void connect();
    void init_modules();

    void on_connect(int);
    void on_disconnect(int);
    void on_message(const struct mosquitto_message* msg);
    void on_log(int, const char*);

    bool load_config(const std::string& path, bool db = true);
    bool save_config(const std::string& path, bool db = true);
    bool load_db();
    bool save_db();

    const std::string&                                 db_path()       const { return m_db_path; }
    int db_save_interval()                                             const { return m_db_save_interval; }
    const std::set<std::string>&                       modules_names() const { return m_modules_names; }
    const std::multimap<std::string, callback_func_t>& callbacks()     const { return m_callbacks; }
    const std::set<func_t>&                            cb_connect()    const { return m_cb_connect; }
    const std::set<func_t>&                            cb_disconnect() const { return m_cb_disconnect; }

    ~MQCentral();


    template<class Archive> void serialize(Archive& ar);

    private:

    std::string m_host = "localhost";
    int m_port = 1883;
    int m_QOS = 0;

    std::string m_db_path = "/var/lib/mqcentral.db";
    int m_db_save_interval = 300;

    std::set<std::string> m_modules_names;
    std::set<std::string> m_modules_path =
    {
        MQC_MODULES_PATH "/mqcmodule-helloworld.so"
    };

    std::set<func_t> m_cb_connect;
    std::set<func_t> m_cb_disconnect;

    std::multimap<std::string, callback_func_t> m_callbacks;
};


extern MQCentral MQC;


#endif
