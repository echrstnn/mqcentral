#ifndef MQC_DB_H
#define MQC_DB_H

#include <mutex>
#include <set>
#include <shared_mutex>
#include <time.h>
#include <unordered_map>
#include <vector>

#include "logger.hpp"
#include "shared_mutex.hpp"


struct DBEntry
{
    time_t time;
    std::string msg;

    template<class Archive> void serialize(Archive& ar);
};


class DBTopic
{
    public:

    // Used internally
    void init_overall (const std::string& topic, int capacity);
    void init_circular(const std::string& topic, int capacity);

    // Insert entries
    void insert         (const DBEntry& entry);
    void insert_overall (const DBEntry& entry);
    void insert_circular(const DBEntry& entry);

    // Get number of entries
    int size();
    int size_overall()  { return m_head_overall; }
    int size_circular() { return m_size_circular; }

    // Get entries
    const DBEntry& get         (int index);
    const DBEntry& get_overall (int index);
    const DBEntry& get_circular(int index);

    // Various functions
    int capacity_overall()     const { return m_capacity_overall; }
    int capacity_circular()    const { return m_capacity_circular; }
    const std::string& topic() const { return m_topic; }
    std::mutex& mutex()              { return m_mutex; }


    // This prevent errors from cereal lib because of non-copyable mutex
    // (cereal uses copy constructor internally)
    DBTopic() {}
    DBTopic(const DBTopic& o) :
        m_topic            (o.m_topic),
        m_capacity_overall (o.m_capacity_overall),
        m_head_overall     (o.m_head_overall),
        m_skip_overall     (o.m_skip_overall),
        m_currskip_overall (o.m_currskip_overall),
        m_capacity_circular(o.m_capacity_circular),
        m_size_circular    (o.m_size_circular),
        m_head_circular    (o.m_head_circular),
        m_overall          (o.m_overall),
        m_circular         (o.m_circular)
    {
        if(!copy_ctor_allowed)
            log_warn("DBTopic copy constructor called. This should not happen.");
    }

    static bool copy_ctor_allowed;


    template<class Archive> void serialize(Archive& ar);


    private:

    std::mutex m_mutex;

    std::string m_topic;

    int m_capacity_overall = 0;
    int m_head_overall     = 0;
    int m_skip_overall     = 1;
    int m_currskip_overall = 0;

    int m_capacity_circular = 0;
    int m_size_circular     = 0;
    int m_head_circular     = 0;

    std::vector<DBEntry> m_overall;
    std::vector<DBEntry> m_circular;
};


class LockedDBTopic
{
    public:

    void lock(DBTopic* dbt, ting::shared_mutex* s_mutex);
    void unlock();

    DBTopic* operator->();

    private:

    DBTopic* m_dbt = NULL;
    std::unique_lock<std::mutex> m_lock;
    ting::shared_lock<ting::shared_mutex> m_shared_lock;
};


class DBClass
{
    public:

    void register_overall (const std::string& topic, int capacity = 2048);
    void register_circular(const std::string& topic, int capacity = 2048);

    void insert(const std::string& topic, const std::string& msg);

    bool get(LockedDBTopic& dbt, const std::string& topic);

    const std::unordered_map<std::string, DBTopic>& db()     const { return m_db; }
    const std::set<std::string>&                    topics() const { return m_topics; };
          ting::shared_mutex&                       mutex()        { return m_mutex; }


    template<class Archive> void serialize(Archive& ar);


    private:

    ting::shared_mutex m_mutex;

    std::unordered_map<std::string, DBTopic> m_db;
    std::set<std::string> m_topics;
};


extern DBClass DB;


#endif
