#ifndef MQC_LOGGER_H
#define MQC_LOGGER_H

#include <atomic>
#include <syslog.h>

extern int MQC_LOG_LEVEL;
extern bool MQC_DEBUG;

extern bool MQC_LOG_STDOUT;
extern bool MQC_LOG_SYSLOG;


class StackLogger
{
    public:

    StackLogger()
    {
        m_depth++;
    }

    ~StackLogger()
    {
        m_depth--;
    }

    static int depth()
    {
        return m_depth;
    }

    private:

    static thread_local int m_depth;
};


void _log_internal(int priority, const char* funcname, const char* format, ...);

#define    log_err(format, ...) _log_internal(LOG_ERR,     __func__, format, ##__VA_ARGS__)
#define   log_warn(format, ...) _log_internal(LOG_WARNING, __func__, format, ##__VA_ARGS__)
#define log_notice(format, ...) _log_internal(LOG_NOTICE,  __func__, format, ##__VA_ARGS__)
#define   log_info(format, ...) _log_internal(LOG_INFO,    __func__, format, ##__VA_ARGS__)
#define  log_debug(format, ...) _log_internal(LOG_DEBUG,   __func__, format, ##__VA_ARGS__)

#endif
