#include "db.hpp"

// Init functions

void DBTopic::init_overall(const std::string& topic, int capacity)
{
    StackLogger _;

    if(m_topic == "")
        m_topic = topic;
    else if(m_topic != topic)
    {
        log_err("Topic names %s and %s does not match", m_topic.c_str(), topic.c_str());
        exit(EXIT_FAILURE);
    }

    if(m_capacity_overall && m_capacity_overall != capacity)
    {
        log_err("Capacity in DB is %d, impossible to resize DB", topic.c_str(), capacity, m_capacity_overall);
        exit(EXIT_FAILURE);
    }

    m_capacity_overall = capacity;
    m_overall.resize(capacity);
}

void DBTopic::init_circular(const std::string& topic, int capacity)
{
    StackLogger _;

    if(m_topic == "")
        m_topic = topic;
    else if(m_topic != topic)
    {
        log_err("Topic names %s and %s does not match", m_topic.c_str(), topic.c_str());
        exit(EXIT_FAILURE);
    }

    if(m_capacity_circular && m_capacity_circular != capacity)
    {
        log_err("register_circular(%s, %d): capacity in DB is %d, impossible to resize DB",
                topic.c_str(), capacity, m_capacity_circular);
        exit(EXIT_FAILURE);
    }

    m_capacity_circular = capacity;
    m_circular.resize(capacity);
}

// Insert entries

void DBTopic::insert(const DBEntry& entry)
{
    if(m_capacity_overall)
        insert_overall(entry);

    if(m_capacity_circular)
        insert_circular(entry);
}

void DBTopic::insert_overall(const DBEntry& entry)
{
    StackLogger _;

    m_currskip_overall++;
    if(m_currskip_overall != m_skip_overall)
    {
        log_debug("Message '%s' from %s discarded by DB (overall mode)", entry.msg.c_str(), m_topic.c_str());
        return;
    }
    m_currskip_overall = 0;

    if(m_head_overall == m_capacity_overall)
    {
        log_debug("Performing compression on DB topic %s (overall mode)", m_topic.c_str());

        m_skip_overall *= 2;

        for(int i = 1; i < m_capacity_overall / 2; i++)
            m_overall[i] = m_overall[2 * i];

        m_head_overall = m_capacity_overall / 2;
    }

    m_overall[m_head_overall] = entry;

    m_head_overall++;

    log_debug("Inserted '%s' in DB topic %s (overall mode)", entry.msg.c_str(), m_topic.c_str());
}

void DBTopic::insert_circular(const DBEntry& entry)
{
    StackLogger _;

    m_circular[m_head_circular] = entry;

    m_head_circular = (m_head_circular + 1) % m_capacity_circular;
    m_size_circular = std::min(m_size_circular + 1, m_capacity_circular);

    log_debug("Inserted '%s' in DB topic %s (circular mode)", entry.msg.c_str(), m_topic.c_str());
}

// Get number of entries

int DBTopic::size()
{
    if(m_capacity_overall && m_capacity_circular)
    {
        log_err("Topic %s registered for both modes, use size_<mode> instead", m_topic.c_str());
        exit(EXIT_FAILURE);
    }
    else if(m_capacity_overall)
        return size_overall();
    else if(m_capacity_circular)
        return size_circular();
    else
    {
        log_err("Topic %s is not registered in DB", m_topic.c_str());
        exit(EXIT_FAILURE);
    }
}

// Get entries

const DBEntry& DBTopic::get(int index)
{
    if(m_capacity_overall && m_capacity_circular)
    {
        log_err("Topic %s registered for both modes, use get_<mode> instead", m_topic.c_str());
        exit(EXIT_FAILURE);
    }
    else if(m_capacity_overall)
        return get_overall(index);
    else if(m_capacity_circular)
        return get_circular(index);
    else
    {
        log_err("Topic %s is not registered in DB", m_topic.c_str());
        exit(EXIT_FAILURE);
    }
}

const DBEntry& DBTopic::get_overall(int index)
{
    return m_overall[index];
}

const DBEntry& DBTopic::get_circular(int index)
{
    return m_circular[(m_head_circular - index - 1 + 2 * m_capacity_circular) % m_capacity_circular];
}
