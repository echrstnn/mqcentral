#include <dlfcn.h>

#include "mqcentral.hpp"

static void* load_symbol(void* handle, const char* name)
{
    StackLogger _;

    void* symbol = dlsym(handle, name);
    const char* dlsym_error = dlerror();
    if(dlsym_error)
    {
        log_err(dlsym_error);
        exit(EXIT_FAILURE);
    }
    return symbol;
}

void MQCentral::init_modules()
{
    StackLogger _;

    for(const std::string& ipath : m_modules_path)
    {
        log_info("Loading module %s", ipath.c_str());

        char* modname = NULL;
        {
            StackLogger _;

            log_debug("Opening %s", ipath.c_str());

            void* handle = dlopen(ipath.c_str(), RTLD_NOW);
            if(handle == NULL)
            {
                log_err(dlerror());
                exit(EXIT_FAILURE);
            }

            log_debug("Loading symbols");

            init_func_t initfunc = (init_func_t) load_symbol(handle, "init");
            modname = (char*) load_symbol(handle, "MODULE_NAME");

            m_modules_names.insert(modname);

            log_debug("Calling init()");

            int rc = initfunc();
            if(rc)
            {
                log_err("init() returned a non-zero value: %d", rc);
                exit(EXIT_FAILURE);
            }
        }

        log_notice("Module %s initialized", modname);
    }
}
