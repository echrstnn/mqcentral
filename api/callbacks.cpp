#include <thread>

#include "mqcentral.hpp"
#include "db.hpp"

void MQCentral::on_connect(int)
{
    log_notice("Connected to %s", m_host.c_str());

    for(auto i : m_callbacks)
    {
        StackLogger _;

        const char* sub = i.first.c_str();
        log_debug("Subscibing to %s", sub);
        int rc =  subscribe(NULL, sub, m_QOS);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            log_err("An error occured while subscribing to %s", sub);
            exit(EXIT_FAILURE);
        }
    }

    for(auto i : DB.topics())
    {
        StackLogger _;

        const char* sub = i.c_str();
        log_debug("Subscibing to %s", sub);
        int rc =  subscribe(NULL, sub, m_QOS);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            log_err("An error occured while subscribing to %s", sub);
            exit(EXIT_FAILURE);
        }
    }

    for(auto icb : m_cb_connect)
    {
        std::thread th(icb);
        th.detach();
    }
}

void MQCentral::on_disconnect(int)
{
    log_warn("Disconnected from %s", m_host.c_str());

    for(auto icb : m_cb_disconnect)
        icb();
}

void MQCentral::on_message(const struct mosquitto_message* msg)
{
    std::string topic = msg->topic;
    std::string pl((char*) msg->payload, msg->payloadlen);

    DB.insert(topic, pl);

    auto lb = m_callbacks.lower_bound(topic);
    auto ub = m_callbacks.upper_bound(topic);

    if(lb != ub)
        log_debug("Message '%s' on topic %s: %d callbacks", pl.c_str(), topic.c_str(), m_callbacks.count(topic));
    else
        log_debug("Message '%s' on topic %s: no callbacks", pl.c_str(), topic.c_str());


    while(lb != ub)
    {
        std::thread th(lb->second, topic, pl);
        th.detach();
        lb++;
    }
}

void MQCentral::on_log(int, const char* _str)
{
    if(!MQC_DEBUG)
        return;

    std::string str(_str);
    bool timemsg = (str.find("PUBLISH") != std::string::npos && str.find("mqc/time") != std::string::npos);
    if(timemsg && str.find("epoch") == std::string::npos)
        return;

    log_debug("[MQTT] %s", _str);

    if(timemsg)
        log_debug("[MQTT] Updating other mqc/time/ topics");
}
