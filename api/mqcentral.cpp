#include <string.h>
#include <unistd.h>

#include "mqcentral.hpp"
#include "db.hpp"


DBClass DB;

MQCentral MQC;


void MQCentral::register_on_connect(func_t func)
{
    m_cb_connect.insert(func);
}

void MQCentral::register_on_disconnect(func_t func)
{
    m_cb_disconnect.insert(func);
}

void MQCentral::register_callback(const std::string& topic, callback_func_t func)
{
    m_callbacks.insert(std::make_pair(topic, func));
}

void MQCentral::publish(const std::string& topic, const std::string& msg, bool retain)
{
    mosqpp::mosquittopp::publish(NULL, topic.c_str(), msg.length(), msg.c_str(), m_QOS, retain);
}

void MQCentral::connect()
{
    log_info("Connecting to %s...", m_host.c_str());

    int rc = mosqpp::mosquittopp::connect(m_host.c_str(), m_port);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        if(rc == MOSQ_ERR_ERRNO)
            log_err(strerror(errno));
        else
            log_err("Unknown error while connecting");
        exit(EXIT_FAILURE);
    }

    loop_forever();
}

MQCentral::~MQCentral()
{
    log_notice("Disconnecting before stopping");
    MQC.disconnect();

    log_info("Waiting for disconnect callbacks");
    usleep(1500 * 1000);

    MQC.save_db();
    log_warn("Stopped MQCentral");
}
