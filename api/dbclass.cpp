#include "db.hpp"

bool DBTopic::copy_ctor_allowed = false;

// Register topics

void DBClass::register_overall(const std::string& topic, int capacity)
{
    m_topics.insert(topic);
    m_db[topic].init_overall(topic, capacity);
}

void DBClass::register_circular(const std::string& topic, int capacity)
{
    m_topics.insert(topic);
    m_db[topic].init_circular(topic, capacity);
}

// Insert in DB

void DBClass::insert(const std::string& topic, const std::string& msg)
{
    DBEntry entry;
    time(&entry.time);
    entry.msg = msg;

    LockedDBTopic dbt;
    if(get(dbt, topic))
        dbt->insert(entry);
}

// Get a DBTopic

bool DBClass::get(LockedDBTopic& dbt, const std::string& topic)
{
    auto it = m_db.find(topic);
    if(it == m_db.end())
        return false;

    dbt.lock(&it->second, &m_mutex);
    return true;
}

// LockedDBTopic implementation

void LockedDBTopic::lock(DBTopic* dbt, ting::shared_mutex* s_mutex)
{
    if(m_dbt != NULL)
    {
        log_err("Cannot re-init a LockedDBTopic that is already locked");
        exit(EXIT_FAILURE);
    }

    if(dbt != NULL)
    {
        m_shared_lock = ting::shared_lock<ting::shared_mutex>(*s_mutex);
        m_lock = std::unique_lock<std::mutex>(dbt->mutex());
        m_dbt = dbt;
    }
}

void LockedDBTopic::unlock()
{
    if(m_dbt)
    {
        m_dbt = NULL;
        m_lock.unlock();
        m_shared_lock.unlock();
    }
}

DBTopic* LockedDBTopic::operator->()
{
    if(m_dbt == NULL)
    {
        log_err("Using operator -> on a LockedDBTopic holding a NULL value");
        exit(EXIT_FAILURE);
    }
    return m_dbt;
}
