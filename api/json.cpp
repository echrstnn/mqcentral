#include <cereal/archives/json.hpp>
#include <cereal/types/set.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>

#include <cereal/archives/portable_binary.hpp>

#include "mqcentral.hpp"
#include "db.hpp"

template<class Archive> void MQCentral::serialize(Archive& ar)
{
    ar(
        CEREAL_NVP(m_host),
        CEREAL_NVP(m_port),
        CEREAL_NVP(m_db_path),
        CEREAL_NVP(m_db_save_interval),
        CEREAL_NVP(m_modules_path)
    );
}

template<class Archive> void DBEntry::serialize(Archive& ar)
{
    ar(
        CEREAL_NVP(time),
        CEREAL_NVP(msg)
    );
}

template<class Archive> void DBTopic::serialize(Archive& ar)
{
    ar(
        CEREAL_NVP(m_topic),
        CEREAL_NVP(m_capacity_overall),
        CEREAL_NVP(m_head_overall),
        CEREAL_NVP(m_skip_overall),
        CEREAL_NVP(m_currskip_overall),
        CEREAL_NVP(m_capacity_circular),
        CEREAL_NVP(m_size_circular),
        CEREAL_NVP(m_head_circular),
        CEREAL_NVP(m_overall),
        CEREAL_NVP(m_circular)
    );
}

template<class Archive> void DBClass::serialize(Archive& ar)
{
    ar(
        CEREAL_NVP(m_db)
    );
}


// Solve linker issue with template function
void __unused_function()
{
    log_err("__unused_function() called: should not happen. This is a bug.");

    cereal::JSONInputArchive  iar(std::cin);
    cereal::JSONOutputArchive oar(std::cout);
    cereal::PortableBinaryInputArchive  iar2(std::cin);
    cereal::PortableBinaryOutputArchive oar2(std::cout);
    MQC.serialize(iar);
    MQC.serialize(oar);
    DB.serialize(iar2);
    DB.serialize(oar2);

    exit(EXIT_FAILURE);
}
