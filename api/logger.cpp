#include <mutex>
#include <stdarg.h>
#include <stdio.h>
#include <string>

#include "logger.hpp"

thread_local int StackLogger::m_depth = 0;

int MQC_LOG_LEVEL = LOG_INFO;
bool MQC_DEBUG = false;

bool MQC_LOG_STDOUT = true;
bool MQC_LOG_SYSLOG = false;

static std::mutex logmutex;

void _log_internal(int priority, const char* funcname, const char* format, ...)
{
    if(priority <= MQC_LOG_LEVEL)
    {
        std::unique_lock<std::mutex> lock(logmutex);

        va_list args1, args2;
        va_start(args1, format);
        va_copy(args2, args1);

        std::string newformat;
        std::string timestr = (MQC_DEBUG ? std::to_string(time(NULL)) + " " : "");

        switch(priority)
        {
            case LOG_ERR:     newformat += "["        + timestr + "\x1B[1;41mERR\x1B[0m] \x1B[1;31m";  break;
            case LOG_WARNING: newformat += "["        + timestr + "\x1B[30;43mWAR\x1B[0m] \x1B[1;33m"; break;
            case LOG_NOTICE:  newformat += "["        + timestr + "\x1B[1mINF\x1B[0m] \x1B[1m";        break;
            case LOG_INFO:    newformat += "["        + timestr + "INF] ";                             break;
            case LOG_DEBUG:   newformat += "[\x1B[2m" + timestr + "DBG\x1B[0m] \x1B[2m";               break;
        }

        newformat += std::string(3 * StackLogger::depth(), ' ');

        if(MQC_DEBUG)
        {
            newformat += funcname;
            newformat += ": ";
        }
        newformat += format;
        newformat += "\x1B[0m";

        if(MQC_LOG_STDOUT)
        {
            vprintf(newformat.c_str(), args1);
            printf("\n");
        }

        if(MQC_LOG_SYSLOG)
            vsyslog(priority, newformat.c_str(), args2);

        va_end(args1);
        va_end(args2);
    }
}
