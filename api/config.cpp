#include <fstream>

#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>

#include <cereal/archives/portable_binary.hpp>

#include "mqcentral.hpp"
#include "db.hpp"

bool MQCentral::load_config(const std::string& path, bool db)
{
    StackLogger _;

    log_info("Loading config from %s", path.c_str());

    std::ifstream arfile(path);
    if(!arfile)
    {
        log_err("%s: %s", path.c_str(), strerror(errno));
        return false;
    }

    cereal::JSONInputArchive ar(arfile);
    serialize(ar);

    log_notice("Config loaded");

    if(db)
        return load_db();
    return true;
}

bool MQCentral::save_config(const std::string& path, bool db)
{
    StackLogger _;

    log_info("Saving config to %s", path.c_str());

    std::ofstream arfile(path);
    if(!arfile)
    {
        log_err("%s: %s", path.c_str(), strerror(errno));
        return false;
    }

    cereal::JSONOutputArchive ar(arfile);
    serialize(ar);

    log_notice("Config saved");

    if(db)
        return save_db();
    return true;
}

bool MQCentral::load_db()
{
    StackLogger _;

    std::unique_lock<ting::shared_mutex> lock(DB.mutex());

    log_info("Loading DB from %s", db_path().c_str());

    std::ifstream arfile(db_path());
    if(!arfile)
    {
        log_err("%s: %s", db_path().c_str(), strerror(errno));
        return false;
    }

    cereal::PortableBinaryInputArchive ar(arfile);
    DBTopic::copy_ctor_allowed = true;
    DB.serialize(ar);
    DBTopic::copy_ctor_allowed = false;

    log_debug("Found %d topics", DB.db().size());
    for(const auto& it : DB.topics())
    {
        LockedDBTopic dbt;
        DB.get(dbt, it);
        log_debug("  - Topic %s (overall: %d/%d, circular: %d/%d)", dbt->topic().c_str(),
            dbt->size_overall(), dbt->capacity_overall(), dbt->size_circular(), dbt->capacity_circular());
    }

    log_notice("DB loaded");
    return true;
}

#include <unistd.h>
bool MQCentral::save_db()
{
    StackLogger _;

    std::unique_lock<ting::shared_mutex> lock(DB.mutex());

    log_info("Saving DB to %s", db_path().c_str());

    std::ofstream arfile(db_path());
    if(!arfile)
    {
        log_err("%s: %s", db_path().c_str(), strerror(errno));
        return false;
    }

    cereal::PortableBinaryOutputArchive ar(arfile);
    DB.serialize(ar);

    log_notice("DB saved");
    return true;
}
