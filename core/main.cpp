#include <iostream>
#include <signal.h>
#include <string.h>
#include <thread>
#include <unistd.h>

#include "db.hpp"

#include "mqc_core.hpp"


static void sigint_handler(int)
{
    log_warn("Catched SIGINT. Quitting...");
    exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
    mosqpp::lib_init();

    mqc_parse_cmdargs(argc, argv);

    if(MQC_BACKGROUND)
        mqc_daemonize();

    // Load config

    log_notice("Starting MQCentral v%d.%d", MQC_VERSION_MAJOR, MQC_VERSION_MINOR);

    log_info("Loading config and DB");

    if(!MQC.load_config(MQC_CONFIG_PATH, false))
    {
        log_err("%s: %s", MQC_CONFIG_PATH.c_str(), strerror(errno));
        exit(EXIT_FAILURE);
    }

    if(!MQC.load_db())
    {
        log_err("%s: %s", MQC.db_path().c_str(), strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Manage program exit

    atexit(mqc_exit);

    struct sigaction sa;
    sa.sa_handler = sigint_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    sigaction(SIGINT, &sa, NULL);

    // Initialize

    log_info("Initializing modules");
    MQC.init_modules();

    mqc_register_callbacks();

    // Display infos

    log_notice("Initialized %d modules:", MQC.modules_names().size());
    for(const auto& it : MQC.modules_names())
        log_info("  - Module %s", it.c_str());


    log_notice("Registered %d callbacks:", MQC.callbacks().size());

    std::map<std::string, int> numcb;
    for(const auto& it : MQC.callbacks())
        numcb[it.first]++;

    for(const auto& it : numcb)
        log_info("  - Topic %s, %d callbacks", it.first.c_str(), it.second);

    log_info("  - Registered %d on_connect callbacks",    MQC.cb_connect().size());
    log_info("  - Registered %d on_disconnect callbacks", MQC.cb_disconnect().size());


    log_notice("Registered %d topics in DB:", DB.topics().size());
    for(const auto& it : DB.topics())
    {
        LockedDBTopic dbt;
        DB.get(dbt, it);

        std::string logstr = "  - Topic " + dbt->topic();
        if(dbt->capacity_overall())
            logstr += ", overall[" + std::to_string(dbt->capacity_overall()) + "]";
        if(dbt->capacity_circular())
            logstr += ", circular[" + std::to_string(dbt->capacity_circular()) + "]";
        log_info(logstr.c_str());
    }


    // Run

    std::thread th(mqc_timethread);
    th.detach();

    MQC.connect();

    StackLogger _;

    log_warn("Stopped MQTT, quitting");

    usleep(25 * 1000 * 1000);

    exit(EXIT_FAILURE);
}
