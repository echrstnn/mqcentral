#include <string.h>
#include <unistd.h>

#include "mqc_core.hpp"

bool MQC_EXIT = false;

void mqc_exit()
{
    MQC_EXIT = true;
    usleep(180 * 1000);
}

static void mqc_cb_exit(const std::string& topic, const std::string& msg)
{
    StackLogger _;

    if(msg == "1")
    {
        log_notice("Received exit message %s on %s, saving DB.", msg.c_str(), topic.c_str());
        exit(EXIT_SUCCESS);
    }
    else
        log_warn("Received %s on %s. Send '1' to exit MQCentral", msg.c_str(), topic.c_str());
}

void mqc_register_callbacks()
{
    MQC.register_callback("mqc/exit", mqc_cb_exit);
}
