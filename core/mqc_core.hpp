#ifndef MQC_CORE_H
#define MQC_CORE_H

#include "mqcentral.hpp"

#include <string>

extern std::string MQC_CONFIG_PATH;
extern bool MQC_BACKGROUND;
void mqc_parse_cmdargs(int argc, char* argv[]);

void mqc_daemonize();
void mqc_register_callbacks();
void mqc_timethread();

extern bool MQC_EXIT;

void mqc_exit();

#endif
