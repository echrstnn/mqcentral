#include <thread>
#include <unistd.h>

#include "mqc_core.hpp"

static time_t    TIME_RAW;
static struct tm TIME_TM;

static const int NTOPICS = 8;
static const char SUFFIX[NTOPICS][5] = { "sec", "min", "hour", "wday",
                                         "mday", "yday", "mon", "year" };

static int* details[NTOPICS] = { &TIME_TM.tm_sec,  &TIME_TM.tm_min,  &TIME_TM.tm_hour,
                                 &TIME_TM.tm_wday, &TIME_TM.tm_mday, &TIME_TM.tm_yday,
                                 &TIME_TM.tm_mon,  &TIME_TM.tm_year };

void mqc_timethread()
{
    StackLogger _;

    while(!MQC_EXIT)
    {
        time_t time_new = time(NULL);

        if(time_new != TIME_RAW)
        {
            TIME_RAW = time_new;
            localtime_r(&TIME_RAW, &TIME_TM);

            for(int i = 0; i < NTOPICS; i++)
            {
                std::string topic = "mqc/time/";
                topic += SUFFIX[i];
                std::string msg = std::to_string(*details[i]);

                MQC.publish(topic, msg);
            }

            std::string msg = std::to_string((unsigned long long) TIME_RAW);
            MQC.publish("mqc/time/epoch", msg);

            if(time_new % MQC.db_save_interval() == 0)
                MQC.save_db();
        }

        usleep(50 * 1000);
    }

    log_warn("Time publish and DB autosave thread stopped");
}
