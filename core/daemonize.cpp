#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "mqc_core.hpp"

void mqc_daemonize()
{
    log_warn("Running as background daemon");
    log_warn("Check syslog to see if something went wrong");

    MQC_LOG_SYSLOG = true;
    MQC_LOG_STDOUT = false;

    pid_t pid = fork();
    if(pid == -1)
    {
        log_err("fork(): %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(pid > 0)
        exit(EXIT_SUCCESS);

    if(setsid() == -1)
    {
        log_err("setsid(): %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    pid = fork();
    if(pid == -1)
    {
        log_err("fork(): %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if(pid > 0)
        exit(EXIT_SUCCESS);

    chdir("/");

    for(int i = sysconf(_SC_OPEN_MAX); i >= 0; i--)
        close(i);
}
