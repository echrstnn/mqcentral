#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "mqc_core.hpp"

std::string MQC_CONFIG_PATH = "/etc/mqcentral.conf";
bool MQC_BACKGROUND = true;

static void mqc_print_help()
{
    printf("MQCentral v%d.%d\n", MQC_VERSION_MAJOR, MQC_VERSION_MINOR);
    puts("");
    puts("See gitlab.com/echrstnn/mqcentral for more documentation");
    puts("");
    puts("Usage:");
    puts("  mqcentral [--newconfig | --newdb] [-v[v]] [--foreground] [-f <config>]");
}

void mqc_parse_cmdargs(int argc, char* argv[])
{
    static const struct option _options[] =
    { { "newconfig",    no_argument,        NULL, 'c' },
      { "newdb",        no_argument,        NULL, 'D' },
      { "file",         required_argument,  NULL, 'F' },
      { "foreground",   no_argument,        NULL, 'g' },
      { "verbose",      no_argument,        NULL, 'v' },
      { "help",         no_argument,        NULL, 'h' },
      { 0,              0,                  0,     0  } };

    bool newconfig = false, newdb = false;

    int lastopt = -1;
    while((lastopt = getopt_long_only(argc, argv, "cDf:Fvh", _options, NULL)) != -1)
    {
        switch(lastopt)
        {
            case 'c': newconfig = true;         break;
            case 'D': newdb = true;             break;
            case 'f': MQC_CONFIG_PATH = optarg; break;
            case 'F': MQC_BACKGROUND = false;   break;
            case 'v': MQC_LOG_LEVEL++;          break;
            case 'h': mqc_print_help();         exit(EXIT_SUCCESS);
            default: puts("Unknown option");    exit(EXIT_FAILURE);
        }
    }

    if(MQC_LOG_LEVEL > LOG_DEBUG)
        MQC_DEBUG = true, MQC_LOG_LEVEL = LOG_DEBUG;

    // Manage --newconfig and --newdb

    if(newconfig)
    {
        if(access(MQC_CONFIG_PATH.c_str(), F_OK) != -1)
        {
            printf("%s already exists, remove it and rerun this command "
                   "if you really want a new config\n", MQC_CONFIG_PATH.c_str());
            exit(EXIT_FAILURE);
        }

        log_info("Writing default config to %s", MQC_CONFIG_PATH.c_str());

        if(!MQC.save_config(MQC_CONFIG_PATH, false))
            exit(EXIT_FAILURE);

        log_info("Default config written");
    }

    if(newdb)
    {
        if(!MQC.load_config(MQC_CONFIG_PATH, false))
        {
            log_err("Failed to parse config contents from %s", MQC_CONFIG_PATH.c_str());
            exit(EXIT_FAILURE);
        }

        if(access(MQC.db_path().c_str(), F_OK) != -1)
        {
            log_warn("DB file %s already exists, if you really want to "
                     "get a new DB remove the file and rerun this command", MQC.db_path().c_str());
            exit(EXIT_FAILURE);
        }

        log_info("Writing empty DB to %s", MQC.db_path().c_str());

        if(!MQC.save_db())
            exit(EXIT_FAILURE);

        log_info("Empty DB written");
    }

    if(newconfig || newdb)
        exit(EXIT_SUCCESS);
}
